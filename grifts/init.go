package grifts

import (
	"halimindra/tax-calculator/actions"

	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
