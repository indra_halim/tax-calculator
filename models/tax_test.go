package models_test

import (
	"halimindra/tax-calculator/models"
	"testing"
)

func Test_Tax(t *testing.T) {
	tax := models.Tax{
		Name:    "Item 1",
		TaxCode: "1",
		Price:   999999,
	}

	if err := models.DB.Create(&tax); err != nil {
		t.Error(err)
	}
}
