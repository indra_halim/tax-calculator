package actions

func (as *ActionSuite) Test_TaxesResource_List() {
	res := as.HTML("/").Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Bill")
}

func (as *ActionSuite) Test_TaxesResource_Show() {
	as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_TaxesResource_New() {
	res := as.HTML("/taxes").Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "New Tax")
}

func (as *ActionSuite) Test_TaxesResource_Create() {
	as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_TaxesResource_Edit() {
	as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_TaxesResource_Update() {
	as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_TaxesResource_Destroy() {
	as.Fail("Not Implemented!")
}
