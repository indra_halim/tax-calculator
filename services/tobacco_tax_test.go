package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewTobaccoTax(t *testing.T) {
	tt := NewTobaccoTax(10000)

	assert.Equal(t, tt.Type, TypeTobacco)
	assert.NotNil(t, tt.Refundable)
	assert.NotZero(t, tt.Price)

	assert.NotZero(t, tt.GetTaxAmount)
	t.Logf("%+v", tt)
}
