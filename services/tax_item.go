package services

// constants
const (
	TypeTobacco       = "Tobacco"
	TypeFood          = "Food & Beverage"
	TypeEntertainment = "Entertainment"
)

// TaxItemInterface is interface for TaxItem
type TaxItemInterface interface {
	GetTaxAmount() float64
}

// TaxItem base struct for Tax object
type TaxItem struct {
	Code       string
	Type       string
	Refundable bool
	Price      float64
}

// NewTaxItem create new TaxItem instance
func NewTaxItem(code string, price float64) TaxItemInterface {
	switch code {
	case "1":
		return NewFoodTax(price)
	case "2":
		return NewTobaccoTax(price)
	default:
		return NewEntertainmentTax(price)
	}
}

// GetTaxAmount get calculated tax amount
func (ti TaxItem) GetTaxAmount() float64 {
	return 0
}
