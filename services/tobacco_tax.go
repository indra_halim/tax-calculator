package services

// TobaccoTax inherit TaxItem
type TobaccoTax struct {
	TaxItem
}

// NewTobaccoTax create new TobaccoTax instance
func NewTobaccoTax(price float64) TobaccoTax {
	return TobaccoTax{
		TaxItem{
			Code:       "2",
			Type:       TypeTobacco,
			Refundable: false,
			Price:      price,
		},
	}
}

// GetTaxAmount get calculated tax amount
func (tt TobaccoTax) GetTaxAmount() float64 {
	return 10 + (0.02 * tt.Price)
}
