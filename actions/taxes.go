package actions

import (
	"fmt"
	"halimindra/tax-calculator/models"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/pkg/errors"
)

// TaxesResource is the resource for the Tax model
type TaxesResource struct {
	buffalo.Resource
}

// List gets all Taxes. This function is mapped to the path
// GET /taxes
func (v TaxesResource) List(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	taxes := &models.Taxes{}

	// Paginate results. Params "page" and "per_page" control pagination.
	// Default values are "page=1" and "per_page=20".
	q := tx.PaginateFromParams(c.Params())

	// Retrieve all Taxes from the DB
	if err := q.All(taxes); err != nil {
		return errors.WithStack(err)
	}

	// Add the paginator to the context so it can be used in the template.
	c.Set("pagination", q.Paginator)

	return c.Render(200, r.Auto(c, taxes))
}

// Show gets the data for one Tax. This function is mapped to
// the path GET /taxes/{tax_id}
func (v TaxesResource) Show(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Allocate an empty Tax
	tax := &models.Tax{}

	// To find the Tax the parameter tax_id is used.
	fmt.Println(c.Params())
	if err := tx.Find(tax, c.Param("tax_id")); err != nil {
		return c.Error(404, err)
	}

	c.Set("tax", tax)
	return c.Render(200, r.HTML("taxes/show.html"))
}

// New renders the form for creating a new Tax.
// This function is mapped to the path GET /taxes/new
func (v TaxesResource) New(c buffalo.Context) error {
	return c.Render(200, r.Auto(c, &models.Tax{}))
}

// Create adds a Tax to the DB. This function is mapped to the
// path POST /taxes
func (v TaxesResource) Create(c buffalo.Context) error {
	// Allocate an empty Tax
	tax := &models.Tax{}

	// Bind tax to the html form elements
	if err := c.Bind(tax); err != nil {
		return errors.WithStack(err)
	}

	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Validate the data from the html form
	verrs, err := tx.ValidateAndCreate(tax)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		// Make the errors available inside the html template
		c.Set("errors", verrs)

		// Render again the new.html template that the user can
		// correct the input.
		return c.Render(422, r.Auto(c, tax))
	}

	// If there are no errors set a success message
	c.Flash().Add("success", "Tax was created successfully")

	// and redirect to the taxes index page
	return c.Render(201, r.Auto(c, tax))
}

// Edit renders a edit form for a Tax. This function is
// mapped to the path GET /taxes/{tax_id}/edit
func (v TaxesResource) Edit(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Allocate an empty Tax
	tax := &models.Tax{}

	if err := tx.Find(tax, c.Param("tax_id")); err != nil {
		return c.Error(404, err)
	}

	c.Set("tax", tax)
	return c.Render(200, r.HTML("taxes/edit.html"))
}

// Update changes a Tax in the DB. This function is mapped to
// the path PUT /taxes/{tax_id}
func (v TaxesResource) Update(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Allocate an empty Tax
	tax := &models.Tax{}

	if err := tx.Find(tax, c.Param("tax_id")); err != nil {
		return c.Error(404, err)
	}

	// Bind Tax to the html form elements
	if err := c.Bind(tax); err != nil {
		return errors.WithStack(err)
	}

	verrs, err := tx.ValidateAndUpdate(tax)
	if err != nil {
		return errors.WithStack(err)
	}

	if verrs.HasAny() {
		// Make the errors available inside the html template
		c.Set("errors", verrs)

		// Render again the edit.html template that the user can
		// correct the input.
		return c.Render(422, r.Auto(c, tax))
	}

	// If there are no errors set a success message
	c.Flash().Add("success", "Tax was updated successfully")

	// and redirect to the taxes index page
	return c.Render(200, r.Auto(c, tax))
}

// Destroy deletes a Tax from the DB. This function is mapped
// to the path DELETE /taxes/{tax_id}
func (v TaxesResource) Destroy(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Allocate an empty Tax
	tax := &models.Tax{}

	// To find the Tax the parameter tax_id is used.
	if err := tx.Find(tax, c.Param("tax_id")); err != nil {
		return c.Error(404, err)
	}

	if err := tx.Destroy(tax); err != nil {
		return errors.WithStack(err)
	}

	// If there are no errors set a flash message
	c.Flash().Add("success", "Tax was destroyed successfully")

	// Redirect to the taxes index page
	return c.Render(200, r.Auto(c, tax))
}
